---
title: Features
features:
  - name: new
    avatar: /static/images/uploads/img-community.png
    text: test a new feature update
Whitepaper: "[White paper](https://en.wikipedia.org/wiki/White_paper). Au moins
  deux jeux de rôles sur table proposent de jouer des chats. Le premier porte le
  titre de « Cat », de John Wick. Les chats y combattent les terribles boggins
  qui se nourrissent des rêves et des âmes des humains."
---
Kotahi is a state of the art publishing platform that supports multiple workflows including journals, micropubs, and preprints and the radical optimisation of scholarly communications workflows. The following features are built into Kotahi 1.0beta. To see your feature ideas added to Kotahi join the Kotahi community!