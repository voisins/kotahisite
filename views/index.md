---
layout: home.njk
class: home
title: Features
subtitle: Open Source Scholarly Publishing Platform
features:
  - name: API-ready
    text: Integrate with other publishing systems. Kotahi has a full featured API
      and various adapters are available for DB integrations
    avatar: /static/images/uploads/kotahi-api-ready.svg
Whitepaper: "[White paper](https://en.wikipedia.org/wiki/White_paper). Au moins
  deux jeux de rôles sur table proposent de jouer des chats. Le premier porte le
  titre de « Cat », de John Wick. Les chats y combattent les terribles boggins
  qui se nourrissent des rêves et des âmes des humains."
---
Kotahi is a state of the art publishing platform that supports multiple workflows including journals, micropubs, and preprints and the radical optimisation of scholarly communications workflows. The following features are built into Kotahi 1.0beta. To see your feature ideas added to Kotahi join the Kotahi community!