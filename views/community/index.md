---
layout: community.njk
title: Community
person:
  - name: Jean faire
    avatar: /static/images/uploads/pitayacat.jpg
    text: >-
      Le ***lorem ipsum*** (également appelé **faux-texte**, ***lipsum***, ou
            **bolo bolo**[1](https://fr.wikipedia.org/wiki/Lorem_ipsum#cite_note-1))
            est, en [imprimerie](https://fr.wikipedia.org/wiki/Imprimerie
            "Imprimerie"), une suite de mots sans signification utilisée à titre
            provisoire pour
            [calibrer](https://fr.wikipedia.org/wiki/Mise_en_page#Méthodes_de_mise_en_page
            "Mise en page") une [mise en
            page](https://fr.wikipedia.org/wiki/Mise_en_page "Mise en page"), le texte
            définitif venant remplacer le faux-texte dès qu'il est prêt ou que la mise
            en page est achevée
    top: 0
    left: 0
  - name: Jean Défaire
    avatar: /static/images/uploads/toastcat.jpg
    text: >-
      Généralement, on utilise un texte en faux
            [latin](https://fr.wikipedia.org/wiki/Latin "Latin") (le texte ne veut
            rien dire, il a été modifié), le *Lorem ipsum* ou *Lipsum*. L'avantage du
            latin est que l'opérateur sait au premier coup d'œil que la page contenant
            ces lignes n'est pas valide et que l'attention du lecteur n'est pas
            dérangée par le contenu, lui permettant de demeurer concentré sur le seul
            aspect graphique.
    top: 0
    left: 0
  - name: Jean de Fer
    text: Généralement, on utilise un texte en faux
      [latin](https://fr.wikipedia.org/wiki/Latin "Latin") (le texte ne veut
      rien dire, il a été modifié), le *Lorem ipsum* ou *Lipsum*. L'avantage du
      latin est que l'opérateur sait au premier coup d'œil que la page contenant
      ces lignes n'est pas valide et que l'attention du lecteur n'est pas
      dérangée par le contenu, lui permettant de demeurer concentré sur le seul
      aspect graphique.
    avatar: /static/images/uploads/watermelon-cat-hat-costume.jpg
    top: -20
    left: 0
sponsors:
  - name: Kotahi 1
    logo: /static/images/uploads/kotahi-logo-white.svg
  - name: Kotahi 2
    logo: /static/images/uploads/kotahi-logo-white.svg
  - name: Kotahi 3
    logo: /static/images/uploads/kotahi-logo-white.svg
  - name: Kotahi 4
    logo: /static/images/uploads/kotahi-logo-white.svg
  - name: Kotahi 5
    logo: /static/images/uploads/kotahi-logo-white.svg
  - name: Kotahi 6
    logo: /static/images/uploads/kotahi-logo-white.svg
---
Welcome to the Kotahi community. Below are some of the community members that are actively engaged in working with Kotahi. We meet reguarly to work on the project. If you would like to join us then email us at community@kotahi.community
